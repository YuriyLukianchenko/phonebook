package com.lukyanchenko.phonebook.controller;

import com.itextpdf.text.Document;
import com.lukyanchenko.phonebook.domain.CompanyList;
import com.lukyanchenko.phonebook.domain.PhoneNumberList;
import com.lukyanchenko.phonebook.domain.User;
import com.lukyanchenko.phonebook.domain.UserList;
import com.lukyanchenko.phonebook.repository.CompanyRepository;
import com.lukyanchenko.phonebook.repository.PhoneNumberRepository;
import com.lukyanchenko.phonebook.repository.UserRepository;
import com.lukyanchenko.phonebook.service.PdfService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@RestController
public class Controller {

    private UserRepository userRepository;
    private CompanyRepository companyRepository;
    private PhoneNumberRepository phoneNumberRepository;
    private PdfService pdfService;

    public Controller(UserRepository userRepository,
                      CompanyRepository companyRepository,
                      PhoneNumberRepository phoneNumberRepository,
                      PdfService pdfService) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.phoneNumberRepository = phoneNumberRepository;
        this.pdfService = pdfService;
    }

    @PostMapping("/upload")
    public String uploadData(@RequestPart("users")  UserList users,
                             @RequestPart("companies") CompanyList companies,
                             @RequestPart("numbers") PhoneNumberList numbers) {

        users.getUsers().forEach(userRepository::save);
        companies.getCompanies().forEach(companyRepository::save);
        numbers.getNumbers().forEach(phoneNumberRepository::save);

        return "All elements downloaded successfully";
    }

    @GetMapping(value = "/usersPDF",
    produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> getUsersPDF() {
        return new ResponseEntity<>(pdfService.getUsersAsPdf(), HttpStatus.OK);
    }
}
