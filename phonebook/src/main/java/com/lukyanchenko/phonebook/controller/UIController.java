package com.lukyanchenko.phonebook.controller;

import com.lukyanchenko.phonebook.domain.User;
import com.lukyanchenko.phonebook.repository.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UIController {

    private UserRepository userRepository;

    public UIController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/getUsers")
    public ModelAndView getListOfUsers() {
        List<User> users = (List)userRepository.findAll();
        Map<String, List<User>> parameters = new HashMap<>();
        parameters.put("users", users);
        return new ModelAndView("users", parameters);
    }
}
