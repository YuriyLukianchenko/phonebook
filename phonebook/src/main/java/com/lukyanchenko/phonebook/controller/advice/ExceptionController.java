package com.lukyanchenko.phonebook.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView onServerException(Exception e) {

        Map<String, String> parameterrs = new HashMap<>();
        parameterrs.put("errorMessage", e.getMessage());
        return new ModelAndView("error", parameterrs);
    }
}
