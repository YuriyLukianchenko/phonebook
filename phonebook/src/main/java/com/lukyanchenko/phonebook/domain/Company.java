package com.lukyanchenko.phonebook.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "company_id")
    private int id;

    @NotNull
    @Column(name = "company_name", unique = true)
    @JsonProperty("name")
    private String name;

    @OneToMany(mappedBy = "phoneCompany",targetEntity = PhoneNumber.class, cascade = CascadeType.ALL)
    @Column(name = "number_set")
    private Set<PhoneNumber> numberSet;
}
