package com.lukyanchenko.phonebook.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="phone_number")
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="phone_number_id")
    private int id;



    @NotNull
    @Column(name="phone_number_number", unique = true)
    @JsonProperty("number")
    private String number;


    @JoinColumn(name="phone_number_company_name")
    @JsonProperty("phone_company")
    @ManyToOne(targetEntity = Company.class, cascade = CascadeType.ALL)
    private Company phoneCompany;


    @JoinColumn(name="phone_number_user")
    @ManyToOne(targetEntity = User.class, cascade = CascadeType.PERSIST)
    private User user;

    public PhoneNumber(String number, Company phoneCompany, User user) {
        this.number = number;
        this.phoneCompany = phoneCompany;
        this.user = user;
    }
}
