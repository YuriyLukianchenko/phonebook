package com.lukyanchenko.phonebook.domain;

import lombok.*;

import javax.validation.Valid;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhoneNumberList {
    @Valid
    private List<PhoneNumber> numbers;
}
