package com.lukyanchenko.phonebook.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "usr")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_id")
    @JsonProperty("user_id")
    private int id;

    @NotNull
    @Column(name = "usr_name")
    @JsonProperty("user_name")
    private String name;

    @NotNull
    @Column(name = "usr_surname", unique = true)
    @JsonProperty("user_surname")
    private String surname;


    @Column(name = "user_phone_numbers")
    @JsonProperty("user_phone_numbers")
    @OneToMany(targetEntity = PhoneNumber.class, cascade = CascadeType.ALL, mappedBy = "user")
    private List<PhoneNumber> phoneNumbers = new ArrayList<>();

    public User(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
}
