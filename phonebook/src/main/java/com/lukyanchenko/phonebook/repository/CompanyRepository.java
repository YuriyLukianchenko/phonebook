package com.lukyanchenko.phonebook.repository;

import com.lukyanchenko.phonebook.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Integer> {
}
