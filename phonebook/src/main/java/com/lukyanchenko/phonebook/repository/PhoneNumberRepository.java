package com.lukyanchenko.phonebook.repository;

import com.lukyanchenko.phonebook.domain.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Integer> {
}
