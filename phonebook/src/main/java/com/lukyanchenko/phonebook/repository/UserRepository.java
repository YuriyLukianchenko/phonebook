package com.lukyanchenko.phonebook.repository;

import com.lukyanchenko.phonebook.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}
