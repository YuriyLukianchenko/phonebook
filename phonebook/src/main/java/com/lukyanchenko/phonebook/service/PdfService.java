package com.lukyanchenko.phonebook.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.lukyanchenko.phonebook.domain.User;
import com.lukyanchenko.phonebook.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

@Service
@Slf4j
public class PdfService {

    private UserRepository userRepository;

    public PdfService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public byte[] getUsersAsPdf() {

        List<User> usersList = (List) userRepository.findAll();

        ByteArrayOutputStream baos  = new ByteArrayOutputStream();
        Document pdfList = new Document();
        try {
            PdfWriter.getInstance(pdfList, baos);
        } catch (DocumentException e) {
            log.error("Document exception occurred");
        }

        pdfList.open();
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.GREEN);
        Paragraph paragraph = new Paragraph();
        usersList.stream()
                .map(element -> new Chunk(element.getName() + ' ' + element.getSurname() + '\n', font))
                .forEach(element1 -> {
                        paragraph.add(element1);
                        paragraph.add(Chunk.NEWLINE);
                });
        try {
            pdfList.add(paragraph);
        } catch (DocumentException e) {
            log.error("Failed to set paragraph in document");
        }
        pdfList.close();

        return baos.toByteArray();
    }
}
